$(document).ready(function() {
  AOS.init( {
    // uncomment below for on-scroll animations to played only once
    // once: true  
  }); // initialize animate on scroll library
});

// copy phone and mail to clipboard
// copy phone and mail to clipboard
$('a[href^=mailto]').click(function() {
  return false;
})
$('a[href^=tel]').click(function() {
  return false;
})


// Add class to mailto and tel links
// Needed to separate the disabling of the default action AND copy email to clipboard
$('a[href^=mailto]').addClass('mailto-link');
$('a[href^=tel]').addClass('tel-link');


var mailto = $('.mailto-link');
var tel = $('.tel-link');
var messageCopy = "Copy text"
var messageSuccess = 'Copied to clipboard';


mailto.append('<span class="message"></span>');
tel.append('<span class="message"></span>');
// On click, get href and remove 'mailto:' from value
// Store email address in a variable.
mailto.click(function() {
  var href = $(this).attr('href');
  var email = href.replace('mailto:', '');
  copyToClipboard(email);
  $('.message').empty().append(messageSuccess);
  setTimeout(function() {
    $('.message').empty().append(messageCopy);}, 2000);
});


tel.click(function() {
  var href = $(this).attr('href');
  var tel = href.replace('tel:', '');
  copyToClipboard(tel);
  $('.message').empty().append(messageSuccess);
  setTimeout(function() {
    $('.message').empty().append(messageCopy);}, 2000);
});


// Copies the email variable to clipboard
function copyToClipboard(text) {
  var dummy = document.createElement("input");
  document.body.appendChild(dummy);
  dummy.setAttribute('value', text);
  dummy.select();
  document.execCommand('copy');
  document.body.removeChild(dummy);
}


// Smooth scroll for links with hashes
$('a.smooth-scroll')
    .click(function(event) {
      // On-page links
      if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
          &&
          location.hostname == this.hostname
      ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (target.length) {
          // Only prevent default if animation is actually gonna happen
          event.preventDefault();
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000, function() {
            // Callback after animation
            // Must change focus!
            var $target = $(target);
            $target.focus();
            if ($target.is(":focus")) { // Checking if the target was focused
              return false;
            } else {
              $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
              $target.focus(); // Set focus again
            };
          });
        }
      }
    });


// delete transparency of navbar when not header
$(function () {
  $(document).scroll(function () {
    var $nav = $(".navbar");
    $nav.toggleClass('scrolled', $(this).scrollTop() > $(".page-header").height());
  });
});

var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
  var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("navbar").style.top = "0";
  } else {
    document.getElementById("navbar").style.top = "-90px";
  }
  prevScrollpos = currentScrollPos;
}